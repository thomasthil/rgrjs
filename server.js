import fs from 'fs';
import express from 'express';
import Schema from './data/schema';
import GraphQLHTTP from 'express-graphql';
import {graphql} from 'graphql';
import {introspectionQuery} from 'graphql/utilities';

import {MongoClient} from 'mongodb';

let app = express();
app.use(express.static('public'));

let MONGO_URL = "mongodb://thomas:int0x80@ds015928.mlab.com:15928/rgrjs";
let PORT = 3000;

(async () => {
    try {
        let db = await MongoClient.connect(MONGO_URL);

        let schema = Schema(db);

        app.use('/graphql', GraphQLHTTP({
            schema,
            graphiql: true
        }));


        let server = app.listen(PORT || 3000, () => {
            console.log(`Server is running on port ${server.address().port}`);
        });

        // Generate schema.json

        let json = await graphql(schema, introspectionQuery);
        fs.writeFile('./data/schema.json', JSON.stringify(json, null, 2), err => {
            if (err) throw err;

            console.log("JSON schema file created");
        });

    } catch (err) {
        console.log(err.stack);
    }
})();


