/**
 * Created by Thomas on 2/25/16.
 */
export let ActionTypes = {
    RECEIVE_LINKS: 'RECEIVE_LINKS'
};

export let EventTypes = {
    LINKS_CHANGE: 'LINKS_CHANGE'
};