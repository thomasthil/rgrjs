/**
 * Created by Thomas on 2/25/16.
 */
import Flux from "flux";

let AppDispatcher = new Flux.Dispatcher();

export default AppDispatcher;