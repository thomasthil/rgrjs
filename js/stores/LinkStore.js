/**
 * Created by Thomas on 2/25/16.
 */
import AppDispatcher from "../AppDispatcher";
import {ActionTypes} from "../Constants";
import {EventTypes} from "../Constants";
import EventEmitter from "events";

let _links = [];

class LinkStore extends EventEmitter {
    constructor(props) {
        super(props);
        AppDispatcher.register(action => {
           switch (action.actionType) {
               case ActionTypes.RECEIVE_LINKS:
                   console.log("3. In Store");
                   _links = action.links;
                   this.emit(EventTypes.LINKS_CHANGE);
                   break;
               default:
                   // do nothing
           }
        });
    }

    getAllLinks() {
        return _links;
    };

    removelistener(eventType, methodRef) {

    };
}

export default new LinkStore();